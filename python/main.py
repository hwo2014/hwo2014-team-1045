from pprint import pprint as pp
from threading import Thread
import json
import math
import random
import socket
import sys
import time

def bound(val, lower, upper):
    # Ensure val is between lower & upper
    return min(max(val, lower), upper)

class COCBot(Thread):
    TRACKS = (
        'keimola',
        'germany',
        'usa',
        'france',
    )

    # Piece types
    STRAIGHT = 1
    CURVE = 2

    # Speed related constants
    MIN_SPEED = 4.0
    MAX_SPEED = 10.0

    # Start with divisors where we are guaranteed to fail & guarantted to succeed
    RADIUS_DIVISOR_MIN = 12.0
    RADIUS_DIVISOR_MAX = 22.0
    @property
    def RADIUS_DIVISOR(self):
        return (self.RADIUS_DIVISOR_MIN + self.RADIUS_DIVISOR_MAX) / 2.0

    # Control how much we accelerate/brake to reach the correct speed
    SPEED_DIFF_MIN = -0.1
    SPEED_DIFF_MAX = 0.1
    SPEED_DIFF_RANGE = SPEED_DIFF_MAX - SPEED_DIFF_MIN

    # These get set later
    color = 'Unknown'
    last_in_piece_distance = None
    last_speed = None
    last_switch_check_piece_index = None
    turbo_available = False
    crashed = False
    lap_crashed = False
    game_started = False
    game_id = None
    game_tick = None
    qualifying_complete = False

    # If we're outputting stats, this is the format we use
    stats_flo = None
    STATS_TEMPLATE = '\t'.join((
        '{game_id}',
        '{game_tick}',
        '{color}',
        '{piece_index}',
        '{lane_index}',
        '{lane_length}',
        '{lane_radius}',
        '{in_piece_distance}',
        '{slip}',
        '{rsd_min}',
        '{rsd_max}',
        '{speed}',
        '{throttle}',
    )) + '\n'


    def __init__(self, host, port, name, key, record_stats=False):
        super(COCBot, self).__init__()

        # Die on ctrl-c
        self.daemon= True

        # Save these for later
        self.bot_name = name
        self.bot_key = key

        # Connect with the given parameters
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, int(port)))

        self.record_stats = record_stats
        if record_stats:
            self.stats_flo = open('./stats.csv', 'a')
            # Add a header
            self.output_stats(
                game_id='game_id',
                game_tick='game_tick',
                color='color',
                piece_index='piece_index',
                lane_index='lane_index',
                lane_length='lane_length',
                lane_radius='lane_radius',
                in_piece_distance='in_piece_distance',
                slip='slip',
                rsd_min='rsd_min',
                rsd_max='rsd_max',
                speed='speed',
                throttle='throttle',
            )

    EXCLUDE_COLORS = []
    def log(self, msg):
        if self.color in self.EXCLUDE_COLORS:
            return
        print('{color}@{game_tick}: {msg}'.format(
            color=self.color,
            game_tick=self.game_tick,
            msg=msg,
        ))

    def output_stats(self, **stats):
        output = self.STATS_TEMPLATE.format(**stats)
        self.stats_flo.write(output)

    def send_message(self, msg_type, data):
        output = {
            'msgType': msg_type,
            'data': data
        }
        if self.game_tick is not None:
            output['gameTick'] = self.game_tick
        self.socket.sendall(json.dumps(output) + '\n')

    def quick_race(self):
        self.log('  <-  join')
        return self.send_message(
            'join',
            {
                'name': self.bot_name,
                'key': self.bot_key
            }
        )

    def join_race(self, players=1, track=None, password=None):
        if not track:
            track = random.choice(self.TRACKS)
        self.log('  <-  joinRace')
        data = {
            'botId': {
                'name': self.bot_name,
                'key': self.bot_key
            },
            'trackName': track,
            'carCount': players
        }
        if password:
            data['password'] = password
        return self.send_message(
            'joinRace',
            data
        )

    def run(self):
        msg_map = {
            # Useful events
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            # Ignored events
            'finish': self.on_ignored,
            'join': self.on_ignored,
            'joinRace': self.on_ignored,
            'tournamentEnd': self.on_ignored,
            'turboStart': self.on_ignored,
            'turboEnd': self.on_ignored,
        }
        socket_file = self.socket.makefile()
        while True:
            line = socket_file.readline()
            if not line:
                break
            # Read the message
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            # Get the gameTick because it not in data
            self.game_tick = msg and msg.get('gameTick', None)
            self.game_id = msg and msg.get('gameId', None)

            # Call the handler
            func = msg_map.get(msg_type, self.on_unknown)
            ret = func(msg_type, data)

            # Only respond to some things
            if msg_type == 'gameStart' or (msg_type == 'carPositions' and self.game_tick is not None):
                ret = ret or ('ping', {})
            elif ret:
                raise Exception('Doesnt look like you should be replying right now (%s and no gameTick)?!' % msg_type)
            # Give the response
            if ret:
                self.send_message(ret[0], ret[1])

    def on_your_car(self, msg_type, data):
        self.log('  ->  yourCar ({color})'.format(
            color=data['color']
        ))
        self.color = data['color']

    def on_game_init(self, msg_type, data):
        self.log('  ->  gameInit ({track})'.format(
            track=data['race']['track']['name']
        ))
        #pp(data)

        # Work out neighbouring lanes
        lanes = data['race']['track']['lanes']
        lanes.sort(key=lambda l: l['distanceFromCenter'])
        # Lanes is now left-right (-ve = left of center, +ve = right)
        self.lanes = {}
        for i in xrange(len(lanes)):
            lane = lanes[i]
            lane['left'] = None
            if i > 0:
                lane['left'] = lanes[i-1]
            lane['right'] = None
            if i < len(lanes) - 1:
                lane['right'] = lanes[i+1]
            self.lanes[lane['index']] = lane

        # Analyse the pieces
        self.pieces = []
        prev_curve = {
            'total_angle': 0,
            'pieces': [],
        }
        for piece in data['race']['track']['pieces']:
            piece_data = {
                'index': len(self.pieces),
                'type': None,
                'switch': 'switch' in piece,
                'angle': None,
                'total_angle': None,
                'remaining_angle': None,
                'lanes': {
                    #'length': ...,
                    #'radius': ...,
                },
                'next_piece': None,
            }

            #Work out what this piece is
            if 'length' in piece:
                # Straight
                piece_data['type'] = self.STRAIGHT
                for lane in self.lanes.values():
                    piece_data['lanes'][lane['index']] = {
                        'length': piece['length'],
                        'radius': None,
                    }

                # If the previous piece(s) were a curve, update them
                total_angle = prev_curve['total_angle']
                for prev_piece in prev_curve['pieces']:
                    # Save the total & remaining angle to the piece
                    prev_piece['total_angle'] = total_angle
                    prev_piece['remaining_angle'] = prev_curve['total_angle']
                    # Update the remaining angle
                    prev_curve['total_angle'] -= prev_piece['angle']
                # Empty the list of pieces
                prev_curve['pieces'] = []
            else:
                piece_data['type'] = self.CURVE
                angle_degrees = abs(piece['angle'])
                angle_radians = math.radians(angle_degrees)
                piece_data['angle'] = angle_radians

                # Work out the total curve lengths
                prev_curve['total_angle'] += angle_radians
                prev_curve['pieces'].append(piece_data)

                for lane in self.lanes.values():
                    # Work out the radius we should use
                    if piece['angle'] > 0:
                        # Curve to the right, left track is further away
                        radius = piece['radius'] - lane['distanceFromCenter']
                    else:
                        #Curve to the left, left track is closer
                        radius = piece['radius'] + lane['distanceFromCenter']

                    # Save it...
                    piece_data['lanes'][lane['index']] = {
                        'length': angle_radians * radius,
                        'radius': radius,
                    }

            if self.pieces:
                self.pieces[-1]['next_piece'] = piece_data

            #pp(piece_data)

            self.pieces.append(piece_data)

        if len(prev_curve['pieces']) > 0 or abs(prev_curve['total_angle']) > 0.01:
            pp(prev_curve)
            print('I think we started or ended on a curve :(')

        # Track is a loop
        self.pieces[-1]['next_piece'] = self.pieces[0]

        #Record how many laps there are
        # Qualifying roudns don't have a lap limit...
        self.laps = data['race']['raceSession'].get('laps', 9)
        self.laps_number = 1

    def on_game_start(self, msg_type, data):
        self.log('  ->  gameStart')
        #pp(data)

        # Reset lots of stuff in case we've had qualifying
        self.last_in_piece_distance = None
        self.last_speed = None
        self.last_switch_check_piece_index = None
        self.turbo_available = False
        self.crashed = False
        self.lap_crashed = False

        # Mark the game as started
        self.game_started = True

    def on_car_positions(self, msg_type, data):
        # Ignore carPositions without gameTick
        if self.game_tick is None:
            return

        # Find my data
        for car in data:
            # Check this is my car
            if car['id']['color'] != self.color:
                continue

            # Find the piece I am currently on
            piece_index = car['piecePosition']['pieceIndex']
            piece = self.pieces[piece_index]
            lane_index = car['piecePosition']['lane']['endLaneIndex']
            in_piece_distance = car['piecePosition']['inPieceDistance']
            slip_angle = abs(car['angle'])

            self._update_speed(
                piece,
                lane_index,
                in_piece_distance,
            )

            switch_lane = self._check_switch(
                lane_index,
                piece,
            )
            if switch_lane:
                self.log('  <-  Switch {direction} @ {piece}'.format(
                    piece=piece_index,
                    direction=switch_lane,
                ))
                return 'switchLane', switch_lane

            activate_turbo = self._check_turbo(
                piece
            )
            if activate_turbo:
                self.log('  <-  Activating turbo @ {piece}'.format(
                    piece=piece_index,
                ))
                return 'turbo', 'It\'s turbo time!'

            self._learn_from_crash(
                piece,
                lane_index,
                in_piece_distance,
            )

            throttle = self._get_throttle(
                piece,
                lane_index,
                in_piece_distance,
            )

            # Check slip angle
            if slip_angle > 55.0:
                print 'Angle: %s, Throttle: %s' % (car['angle'], throttle)
                throttle = 0

            if self.record_stats:
                # Record stats
                self.output_stats(
                    game_id=self.game_id,
                    game_tick=self.game_tick,
                    color=self.color,
                    piece_index=piece['index'],
                    lane_index=lane_index,
                    lane_length=piece['lanes'][lane_index]['length'],
                    lane_radius=piece['lanes'][lane_index]['radius'],
                    in_piece_distance=in_piece_distance,
                    slip=car['angle'],
                    rsd_min=self.RADIUS_DIVISOR_MIN,
                    rsd_max=self.RADIUS_DIVISOR_MAX,
                    speed=self.last_speed,
                    throttle=throttle,
                )

            return 'throttle', throttle

        # If we've got here, something's gone wrong!
        pp(data)
        raise Exception('Could\'nt find my car information (car: {color})!'.format(
            color=self.color,
        ))

    def _update_speed(self, piece, lane_index, in_piece_distance):
        """
        Update our calculated speed
        """
        #if piece['lanes'][lane_index]['length'] < in_piece_distance:
        #    self.log('!!!!! Calculated length ({length}) < inPieceDistance ({in_piece_distance}) !!!!!'.format(
        #        length=piece['lanes'][lane_index]['length'],
        #        in_piece_distance=in_piece_distance
        #    ))
        if self.last_in_piece_distance is None:
            # We've just started a new race so our speed should be 0
            self.last_speed = 0
        elif self.last_in_piece_distance < in_piece_distance:
            # We're on the same piece, let's calculate latest speed
            self.last_speed = in_piece_distance - self.last_in_piece_distance
        self.last_in_piece_distance = in_piece_distance

    def _check_switch(self, lane_index, piece):
        """
        Check if we should issue a switch command
        TODO: Describe algorithm
        Find the shortest bath of current_lane, left_lane & right_lane
        """
        # Don't switch right at the start of the round (seems to get ignored)
        in_race = (self.game_tick > 5)
        # Only check if we should switch if we havent checked already
        piece_checked = (self.last_switch_check_piece_index == piece['index'])
        # Only check if we should switch when there is a switch piece next
        switch_next = piece['next_piece']['switch']
        if in_race and (not piece_checked) and switch_next:
            # Check if we should switch
            #self.log('SWITCH CHECK')

            # Make sure we don't recheck this siwtch
            self.last_switch_check_piece_index = piece['index']

            # Check if we should switch to a different track
            my_lane = self.lanes[lane_index]

            # 25% of the time switch randomly - it's like overtaking, but easier!
            if random.random() < 0.25:
                # We want to switch to a random path
                options = [None]
                if my_lane['right']:
                    options.append('Right')
                if my_lane['left']:
                    options.append('Left')
                return random.choice(options)

            # Strategy: Check the distance from this switch to the next switch
            # Include first switch in the distance but not the second switch
            def get_distance(current_piece, lane):
                if not lane:
                    # Make this lane look really long so it will never get chosen
                    return 99999999999
                distance = 0
                while True:
                    # Sum the lengths
                    distance += current_piece['lanes'][lane['index']]['length']

                    # Move to the next piece
                    current_piece = current_piece['next_piece']
                    if current_piece['switch']:
                        break
                return distance

            # Calculate all the distances
            starting_piece = piece['next_piece']['next_piece']
            my_distance = get_distance(starting_piece, my_lane)
            left_distance = get_distance(starting_piece, my_lane['left'])
            right_distance = get_distance(starting_piece, my_lane['right'])

            # Work out what to do
            if my_distance <= left_distance and my_distance <= right_distance:
                # We are already on the shortest track, don't change!
                return None

            if left_distance <= right_distance:
                # Left is shorter
                return 'Left'

            # Right is shortest
            return 'Right'


    def _check_turbo(self, piece):
        # Check if turbo is available
        if self.turbo_available and not self.lap_crashed:
            # Check how much straight we have
            straight_distance = 0
            while piece['type'] == self.STRAIGHT:
                straight_distance += piece['lanes'][0]['length']
                piece = piece['next_piece']

            if straight_distance >= 450:
                self.turbo_available = False
                return True

    def _learn_from_crash(self, piece, lane_index, in_piece_distance):
        """
        Update the speed for the given piece of track based on whether we
        have crashed
        """
        if self.crashed:
            # Be less aggressive
            old = self.RADIUS_DIVISOR

            ## Maybe how far into the piece we are is relevant?
            #total_distance = piece['lanes'][lane_index]['length']
            #remaining_distance = total_distance - in_piece_distance
            #to_add = 0.5 + (remaining_distance / total_distance) * (self.laps_number + 1)
            #self.RADIUS_DIVISOR = self.RADIUS_DIVISOR + to_add #self.laps_number

            # Maybe we should just change by a constant?
            #self.RADIUS_DIVISOR = self.RADIUS_DIVISOR + self.laps_number * self.RADIUS_INCREMENT

            # Binary search for radius_divisor - we've crashed so move the minimum up
            self.RADIUS_DIVISOR_MIN = self.RADIUS_DIVISOR

            # Check we havent gone too far
            if self.RADIUS_DIVISOR_MAX - self.RADIUS_DIVISOR_MIN < 0.1:
                # Hmm the bounds are now too close
                # We have to make them bigger and carry on searching
                self.RADIUS_DIVISOR_MAX = self.RADIUS_DIVISOR_MIN + 0.2

            # Log what's happened
            self.log('Crashed @ {piece}:{lane_index} ({in_piece_distance:.2f} of {piece_length:.2f}): {old:.2f} -> {new:.2f} ({min:.2f} : {max:.2f})'.format(
                piece=piece['index'],
                lane_index=lane_index,
                in_piece_distance=in_piece_distance,
                piece_length=piece['lanes'][lane_index]['length'],
                old=old,
                new=self.RADIUS_DIVISOR,
                min=self.RADIUS_DIVISOR_MIN,
                max=self.RADIUS_DIVISOR_MAX,
            ))

            # Don't check again until we crash again
            self.crashed = False

    def _get_throttle(self, starting_piece, lane_index, in_piece_distance):
        """
        Work out what throttle we should send to the server
        """
        # Work out what I should do

        def get_speed(piece):
            """
            Work out what speed we want to go on the given piece in the current lane
            """
            if piece['type'] == self.STRAIGHT:
                # This is a straight, just go full out
                return self.MAX_SPEED

            # This is a curve; look at the radius to work out the correct speed
            speed = piece['lanes'][lane_index]['radius'] / self.RADIUS_DIVISOR

            # Also, take the angle of the curve into account
            # 45=1.25, 90=1.0, 135=0.75
            angle_multiplier = 1.5 - (piece['total_angle'] / math.pi)
            angle_multiplier = bound(angle_multiplier, 0.9, 1.1)
            #print angle_multiplier
            speed *= angle_multiplier

            return speed

        # Work out what speed we should use based on the next piece
        # Use the speed of the next piece
        desired_speed = get_speed(starting_piece['next_piece'])

        # Make sure the speed is reasonable
        desired_speed = bound(desired_speed, self.MIN_SPEED, self.MAX_SPEED)

        # Work out what I should do
        speed_diff = desired_speed - self.last_speed
        throttle = (speed_diff - self.SPEED_DIFF_MIN) / self.SPEED_DIFF_RANGE
        throttle = bound(throttle, 0, 1)

        return throttle

    def on_crash(self, msg_type, data):
        self.log('  ->  crash ({color})'.format(
            color=data['color'],
        ))
        if data['color'] == self.color:
            self.last_speed = 0
            self.crashed = True
            self.lap_crashed = True

    def on_spawn(self, msg_type, data):
        # Any turbo's received before a spawn are invalid
        if data['color'] == self.color:
            self.turbo_available = False

    def on_turbo_available(self, msg_type, data):
        self.log('  ->  Turbo is available!')
        self.turbo_available = True

    def on_lap_finished(self, msg_type, data):
        self.log('  ->  lapFinished({color})'.format(
            color=data['car']['color'],
        ))
        #pp(data)
        if data['car']['color'] == self.color:
            # Update how many laps we have completed
            self.laps_number += 1
            if self.lap_crashed:
                # We crashed :(
                self.lap_crashed = False
            elif not self.qualifying_complete:
                # WE didn't crash :)
                # Be more aggressive
                old = self.RADIUS_DIVISOR

                # Binary search for radius_divisor - we didnt crash so move the maximum down
                self.RADIUS_DIVISOR_MAX = self.RADIUS_DIVISOR

                # Log what's happened
                self.log('No crash: {old:.2f} -> {new:.2f} ({min:.2f} : {max:.2f})'.format(
                    old=old,
                    new=self.RADIUS_DIVISOR,
                    min=self.RADIUS_DIVISOR_MIN,
                    max=self.RADIUS_DIVISOR_MAX,
                ))


    def on_game_end(self, msg_type, data):
        self.log('  ->  gameEnd')
        self.game_started = False

        # If there are multiple races, first race is always qualifying
        self.qualifying_complete = True

        # Find my best lap (so I dont have to wait for hwo.com)
        for best_lap in data['bestLaps']:
            if best_lap['car']['color'] == self.color:
                self.log('Best lap: {time:.3f}s'.format(
                    time=best_lap['result']['millis']/1000.0,
                ))
                break

    def on_error(self, msg_type, data):
        self.log('  ->  error')
        pp(data)

    def on_ignored(self, msg_type, data):
        pass

    def on_unknown(self, msg_type, data):
        self.log('Got unknown message type: {msg_type}'.format(
            msg_type=msg_type,
        ))
        pp(data)

def quick_race(host, port, name, key):
    """
    Just run a quick_race (mainly for CI)
    """
    bot = COCBot(
        host,
        port,
        name,
        key,
    )

    # Start tht bot
    bot.start()

    # Join the race
    bot.quick_race()

    # Wait for all bots to be finished
    while bot.isAlive():
        time.sleep(1)

def run_game(host, port, name, key, track, cocbot_count=1, total_count=None, password='COCBotIsAwesome', record_stats=False, sync=True):
    bots = []
    # Create all bots
    for i in xrange(cocbot_count):
        # Create the bot
        bot = COCBot(
            host,
            port,
            '%s %s' % (name, i),
            key,
            record_stats=record_stats,
        )
        bots.append(bot)

        # Start tht bot
        bot.start()

        # Join the race
        bot.join_race(
            players=total_count or cocbot_count,
            track=track,
            password=password,
        )

        # Wait a bit to make sure the server has registered the game
        time.sleep(0.5)

    if sync:
        # Wait for all bots to be finished
        while [bot for bot in bots if bot.isAlive()]:
            time.sleep(1)
    else:
        # We'll wait somewhere else...
        return bots


def main(args):
    if len(args) < 5 or len(args) > 6:
        print('Usage: ./run host port botname botkey [mode]')
        return

    host, port, name, key = args[1:5]
    mode = 'quick_race'
    if len(args) == 6:
        mode = args[5]
    print('Running with: host={0}, port={1}, bot name={2}, key={3}'.format(*args[1:5]))

    if mode == 'quick_race':
        quick_race(
            host,
            port,
            name,
            key,
        )

    elif mode == 'test_single':
        run_game(
            host,
            port,
            name,
            key,
            track='keimola',
            # record_stats=True,
        )

    elif mode == 'test_tracks':
        all_bots = []
        for track in COCBot.TRACKS:
            all_bots.extend(
                run_game(
                    host,
                    port,
                    name,
                    key,
                    track=track,
                    record_stats=True,
                    sync = False
                )
            )
        while [bot for bot in all_bots if bot.isAlive()]:
            time.sleep(1)

    elif mode == 'test_tracks_8':
        for track in COCBot.TRACKS:
            run_game(
                host,
                port,
                name,
                key,
                track=track,
                cocbot_count=8,
            )

    elif mode == 'test_tracks_4_multi':
        for track in COCBot.TRACKS:
            run_game(
                host,
                port,
                name,
                key,
                track=track,
                cocbot_count=1,
                total_count=4,
            )

    else:
        raise Exception('Unknown mode: %s' % mode)


if __name__ == '__main__':
    main(sys.argv)
