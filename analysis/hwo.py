from pprint import pprint
import json
import urllib2


FILENAME = './hwo.json'

def update_teams():
    """
    Update the teams from https://helloworldopen.com/api/1/teams/search/
    & save to FILENAME
    """
    with urllib2.urlopen('https://helloworldopen.com/api/1/teams/search/') as req:
        body = req.read()
    with open(FILENAME, 'w') as f:
        f.write(body)


def load_teams():
    """
    Load the teams from ./hwo.json
    """
    with open(FILENAME, 'r') as f:
        contents = f.read()
    teams = json.loads(contents)
    return teams


def analyse(teams, key, output_max=10):
    """
    """
    tracked = {}
    for team in teams:
        value = key.format(**team)
        if value not in tracked:
            tracked[value] = 0
        tracked[value] += 1

    items = []
    for k, v in tracked.iteritems():
        items.append((v, k))
    items.sort()
    items.reverse()
    output_max = output_max or len(items)
    items = items[:output_max]
    print 'Top %s "%s":' % (output_max, key.replace('{', '').replace('}', ''))
    pprint(items)


def main():
    #update_teams()
    teams = load_teams()
    for key in (u'{city}', u'{language}', u'{flagName}', u'{flagName}-{language}'):
        analyse(teams, key)
        print


if __name__ == '__main__':
    main()
